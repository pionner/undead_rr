<?php

function findValue($name, $fullInput)
{
	$start = strpos($fullInput, $name) + strlen($name) + 1;
	
	if(strpos(substr($fullInput, $start, 2), '[')!== false)
		$delimiter = ']';
	else
		$delimiter = ',';
		
	$end = strpos($fullInput, $delimiter, $start);
	
	$val = substr($fullInput, $start, ($end - $start));
	
	$val = trim(str_replace("[", "", $val));
	
	return $val;
}

?>