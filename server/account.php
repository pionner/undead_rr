<?php
function login($data, $sql, $config)
{
	$hashpass = hash("sha512", $data["password"] . $config['passwords_salt']);
	
	$exist=mysqli_fetch_array(mysqli_query($sql, 'SELECT COUNT(*) AS e FROM accounts WHERE login = "'.$data['membername'].'" AND passwd = "'.$hashpass.'"'));
	
	if($exist['e']=='1')
	{
		$ban=mysqli_fetch_array(mysqli_query($sql, 'SELECT ban, ban_reason, banned_by FROM accounts WHERE login = "'.$data['membername'].'"'));
		
		if($ban['ban']==1 && !$config['allow_banned_login'])
		{
			echo '[#err:"because you are banned by '.$ban['banned_by'].' for '.$ban['ban_reason'].'."]';
			return;
		}
		
		$sid=rand(10000000,99999999);
		mysqli_query($sql, 'DELETE FROM sessions WHERE login="'.$data['membername'].'"');
		mysqli_query($sql, 'INSERT INTO sessions (login, sid) VALUES ("'.$data['membername'].'", '.$sid.')');
		echo '[#err:0,#sid:'.$sid.']';
	}
	else
	{
		echo '[#err:-1]';
	}
}

function logout($data, $sql, $config)
{
	mysqli_query($sql, 'DELETE FROM sessions WHERE login="'.$data['membername'].'"');
	echo "[#status:0]";
}

function createAccount($data, $sql, $config)
{
	if($config['disable_account_creator'])
	{
		echo '[#err:"because registration of new accounts is blocked on this server."]';
		return;
	}
	
	if(strlen($data["password"])<$config['minimum_password_length'])
	{
		echo '[#err:"because password is to short."]';
		return;
	}
	
	$hashpass = hash("sha512", $data["password"] . $config['passwords_salt']);
	$query=mysqli_query($sql, 'SELECT COUNT(id) AS e FROM accounts WHERE login = "'.$data['membername'].'"');
	
	$exist=mysqli_fetch_array($query);
	if($exist['e']==1)
	{
		echo '[#err:"because this user already exists."]';
		return;
	}
	
	mysqli_query($sql, 'INSERT INTO accounts (login, passwd) VALUES ("'.$data['membername'].'", "'.$hashpass.'")');
	
	$sid=rand(10000000,99999999);
	mysqli_query($sql, 'INSERT INTO sessions (login, sid) VALUES ("'.$data['membername'].'", '.$sid.')');
	echo '[#err:0,#sid:'.$sid.']';
}

function getLoginFromSid($sid, $sql)
{
	$query=mysqli_query($sql, "SELECT login FROM sessions WHERE sid=".$sid);
	$res=mysqli_fetch_array($query);
	return $res['login'];
}
?>