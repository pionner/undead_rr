![logo](http://retro.robotrage.pro/img/logo_txt.png)

### Read this document before setting up the server
# Overview
Undead_rr is a rewritten server for Robot Rage 2: Rearmed. It is by no means perfect, but it does what it's designed to do.

The server consists of 3 parts, 2 of them are from us:

 - Main server
 - IRC server
 - AuthBot
 
 It does contain new anticheat, that can filter out and ban 

## Main server
Main server manages all the accounts, bots etc. It's written in PHP, and needs some kind of webserver with support for .htaccess rules to run on. Our pick is Apache, but you can use anything as long as it supports .htaccess.
Other than that you will need MySQL/MariaDB database.

## IRC server
Any IRC server will do, however we had some issues with servers other than UnrealIRCd.
Server's motd has to contain word "PONG" for client to connect correctly- this is our workaround for client pinging the authorization entity and expecting the response. 

## AuthBot
Written in Python3, requires it to run. It's purpose is to filter the IRC server from users connecting from other Main servers or other IRC clients.
> **ProTip**: AuthBot is not required for everything to run, but user connecting from other IRC clients will hang up the game for anyone who select them in lobby.

# Setup guide
## Preparation
You will need a VPS or dedicated server. You need access to bash or equivalent, FTP (or any other way to transfer files). You can of course setup it on your own machine, but it would require additional port forwarding, which I will not cover here. I assume you are running Linux Debian or its derivative on your server- you can use any other Linux distro, but some commands might differ. You can also use Windows, but you will have to figure out the differences in steps on your own.
I assume you know your way around Linux systems, can use SSH and FTP.

You need to install Apache, Python 3, MariaDB:

    sudo apt-get update
    sudo apt-get install apache2 mariadb-server python3

[TODO: finish the guide]


---
[Zombie hand icon](https://www.iconfinder.com/roundicons)